<?php 
foreach($post as $data): ?>
<a href="<?php echo base_url().'single_blog/'; ?><?php echo $data->id; ?>">
    <div class="post">
        <div class="post_thumb">
            <img src="<?php echo base_url().'upload/'.$data->f_image; ?>">
        </div>
        <div class="post_content">
            <div class="post_title">
                <h3><?php echo $data->article_title; ?></h3>
                <small class="meta">Posted By <?php echo $data->fname; ?>, <?php echo date('d M Y', strtotime($data->publish_date)); ?></small>
                <p class="post_body"><?php echo $data->article_body; ?></p>
            </div>
        </div>
    </div>
</a>
<hr>
<?php endforeach; ?>



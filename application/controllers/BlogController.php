<?php 


    class BlogController extends My_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('blog');
        }
        public function index()
        {
           
            $this->data['post'] = $this->blog->posts();
            $this->data['cat'] = $this->blog->category();
            $this->render('home', $this->data);
            
        }
        public function byCategory()
        {
            $id = $this->uri->segment(2);
            $this->data['cat_wise'] = $this->blog->perCategory($id);
            $this->data['cat'] = $this->blog->category();
            // $this->data['recent'] = $this->blog->recentPosts();
            $this->load->view('pagebycategory',$this->data);
        }

        public function singlePage()
        {
            $postId = $this->uri->segment(2);
            $this->data['blog'] = $this->blog->singleBlog($postId);
            $this->data['cat'] = $this->blog->category();
            $this->load->view('single_page', $this->data);
        }
    }

?>
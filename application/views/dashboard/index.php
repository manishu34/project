<?php include('header.php'); ?>

<div class="cards">
    <div class="card1 bg-danger text-white">
        <div class="card_bdy">
            <h3>All Posts</h3>
            <p>View your posts.</p>
            <a href="<?= base_url().'posts'; ?>" class="btn">View</a>
        </div>
    </div>
    <div class="card2 bg-primary text-white">
        <div class="card_bdy">
            <h3>Add Posts</h3>
            <p>Create a new post.</p>
            <a href="<?php echo base_url().'create_post'; ?>" class="btn">Add</a>
        </div>
    </div>
    <div class="card3 bg-success text-white">
        <div class="card_bdy">
            <h3>Categories</h3>
            <p>View all categories.</p>
            <a href="<?= base_url().'view_category'; ?>" class="btn">View</a>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
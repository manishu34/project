</main>
    <sidebar>
        <div class="side_content">
            <div class="category">
                <button class="category_btn">Category</button>
                <div class="divider"></div>
                <ul class="category_list">
                    <?php foreach($cat as $category): ?>
                    <li><a href="<?php echo base_url().'mypage/'; ?><?php echo $category->c_id; ?>"><?php echo $category->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            
        </div>
    </sidebar>
</div>
<!-- Footer -->
    <footer class="page-footer font-small">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
      <a href="" class="text-white"> My Blog Page</a>
    </div> 
    <!-- Copyright -->
  
   </footer>
  <!-- Footer -->
</div>
</body>
 </html>
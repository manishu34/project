<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url().'css/single_page.css';?>">
    <title>Blog</title>
</head>
<body>
<header>
    <div class="content">
        <div class="wrap">
            <div class="logo">
                <a href="#" class="brand">Blog</a>
            </div>
            <div class="navbar_toggler">
                <a href="#" class="nav_icon"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>
            <ul class="nav">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li><a href="<?php echo base_url().'login'; ?>">Login</a></li>
            </ul>
        </div>
    </div>
</header>
<div class="wrapper">
<main>
<?php foreach($blog as $data): ?>
<div class="post">
    <div class="post_thumb">
        <img src="<?php echo base_url().'upload/'.$data->f_image; ?>">
    </div>
    <div class="post_content">
        <div class="post_title">
            <h3><?php echo $data->article_title; ?></h3>
            <small class="meta">Posted By <?php echo $data->fname; ?> |&nbsp; <?php echo date('d M Y', strtotime($data->publish_date)); ?> &nbsp; | <?php echo $data->name; ?></small>
            <p class="post_body mt-5"><?php echo $data->article_body; ?></p>
        </div>
    </div>
</div>
<?php endforeach; ?>
</main>
    <sidebar>
        <div class="side_content">
            <div class="category">
                <button class="category_btn">Category</button>
                <div class="divider"></div>
                <ul class="category_list">
                    <?php foreach($cat as $category): ?>
                    <li><a href="<?php echo base_url().'mypage/'; ?><?php echo $category->c_id; ?>"><?php echo $category->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <!-- <div class="category">
                <button class="category_btn">Recent Post</button>
                <div class="divider"></div>
                <div class="recent_content">
                    <div class="recent_items d-flex">
                        <img src="<?php echo base_url().'upload/1.jpg'; ?>" width="80px">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, autem.</p>
                    </div>
                </div>
            </div> -->
            
        </div>
    </sidebar>
</div>
<!-- Footer -->
    <footer class="page-footer font-small">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
      <a href="" class="text-white"> My Blog Page</a>
    </div> 
    <!-- Copyright -->
  
   </footer>
  <!-- Footer -->
</body>
 </html>
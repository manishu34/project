<?php include('header.php'); ?>

<div class="d-flex w-100">
<h2 style="flex:1;">All Categories</h2>
<a href="<?= base_url().'add_category'; ?>" class="btn btn-primary" style="height:40px;display:block;margin:5px 20px;"><i class="fa fa-plus"></i> Add Category</a>
</div>
<hr>
<div class="data">
<div class="message">
    <?php 
        if($this->session->flashdata('msg') != '')
        {
            if($this->session->flashdata('msg') == 'Category Updated Successfully' || $this->session->flashdata('msg') == 'Category Deleted Successfully'){?>
            <div class="alert bg-success">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
            </div>
    <?php  }
       
    }
    ?>
</div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Category Id</th>
                <th>Category Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <?php foreach($data as $category): ?>
            <tr>
                <td><?php echo $category->c_id ;?></td>
                <td><?php echo $category->name ;?></td>
                <td style="width:100px;text-align:center;">
                    <a href="<?php echo base_url().'update_category/'; ?><?php echo $category->c_id ;?>" class="btn btn-primary"><i class="fa fa-cog"></i></a>
                    <a href="<?php echo base_url().'delete/'; ?><?php echo $category->c_id ;?>" class="btn btn-danger"><i class="fa fa-close"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
    </table>
</div>


<?php include('footer.php'); ?>
<?php include('header.php'); ?>

<h2>Create new Post</h2>
<hr>
<?php //print_r($single_post);exit; ?>
<div class="new_post_form">
    <form action="<?php echo base_url().'DashboardController/doUpdatePost'; ?>" method="POST" enctype="multipart/form-data" class="w-100">
    <?php foreach($single_post as $post): ?>

        <input type="hidden" name="user_id" value="<?php echo $post->id; ?>">
        <label for="title">Title</label>
        <div class="form-group">
            <input type="text" name="title" value="<?php echo $post->article_title; ?>" class="form-control" required>
        </div>
        <label for="sub-title">Sub Title</label>
        <div class="form-group">
            <input type="text" name="sub_title" value="<?php echo $post->sub_title; ?>" class="form-control" required>
        </div>
        <div class="form-group">
            <select name="category" class="form-control">
                <option value="<?php echo $post->c_id; ?>"><?php echo $post->name; ?></option>
                <?php foreach($category as $data): ?>
                    <option value="<?php echo $data->c_id; ?>"><?php echo $data->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <label for="Body">Body</label>
        <div class="form-group">
            <textarea class="form-control" name="content" rows="10"><?php echo $post->article_body; ?></textarea>
        </div>
        <div>
            <img src="<?php echo base_url().'upload/'.$post->f_image; ?>" width="150px">
        </div>
        <div class="form-group">
            <input type="file" name="userfile" value="<?php echo $post->f_image; ?>" class="form-control" required>
        </div>

        <?php endforeach; ?>
        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
        <input type="submit" value="Update" class="btn btn-primary">
    </form>
</div>


<script>
    // CKEDITOR.replace( 'content' );
</script>
<?php include('footer.php'); ?>
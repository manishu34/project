<?php include('header.php'); ?>

<?php if($this->session->flashdata('msg') != ''){
    if($this->session->flashdata('msg') == 'Something went wrong'){
    ?>
<div class="alert bg-danger">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
</div>
    <?php }} ?>
<div class="new_post_form">
    <form action="<?php echo base_url().'DashboardController/doUpdateCategory'; ?>" method="POST" enctype="multipart/form-data" class="w-100">
    <?php foreach($updateCategory as $category): ?>


        <input type="hidden" name="c_id" value="<?php echo $category->c_id; ?>" >
        <label for="category">Name</label>
        <div class="form-group">
            <input type="text" name="c_name" value="<?php echo $category->name; ?>" class="form-control" required>
        </div>
        <?php endforeach; ?>
        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
        <input type="submit" value="Update" class="btn btn-primary">
    </form>
</div>

<?php include('footer.php'); ?>
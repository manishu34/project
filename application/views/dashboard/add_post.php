<?php include('header.php'); ?>

<h2>Create new Post</h2>
<hr>
<div class="message">
    <?php 
        if($this->session->flashdata('msg') != '')
        {
            if($this->session->flashdata('msg') == 'Your post has been published successfully'){?>
            <div class="alert bg-success">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
            </div>
    <?php  }
        else
        {
        ?>
            <div class="alert bg-danger">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
            </div>
    <?php
        }
    }
    ?>
</div>
<div class="new_post_form">
    <form action="<?php echo base_url().'DashboardController/doUpload'; ?>" method="POST" enctype="multipart/form-data" class="w-100">
        <input type="hidden" name="user_id" value="<?php echo $user['id']; ?>">
        <label for="title">Title</label>
        <div class="form-group">
            <input type="text" name="title" class="form-control" required>
        </div>
        <label for="sub-title">Sub Title</label>
        <div class="form-group">
            <input type="text" name="sub_title" class="form-control" required>
        </div>
        <div class="form-group">
            <select name="category" class="form-control">
                <option>- Select Category -</option>
                <?php foreach($category as $data): ?>
                    <option value="<?php echo $data->c_id; ?>"><?php echo $data->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <label for="Body">Body</label>
        <div class="form-group">
            <textarea class="form-control" name="content" rows="10"></textarea>
        </div>
        <div class="form-group">
            <input type="file" name="userfile" class="form-control">
        </div>
        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
        <input type="submit" value="Publish" class="btn btn-primary">
    </form>
</div>


<script>
    // CKEDITOR.replace( 'content' );
</script>
<?php include('footer.php'); ?>
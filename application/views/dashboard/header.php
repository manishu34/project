<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url().'css/dashboard.css'; ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
    
    <!-- <link rel="stylesheet" href=""> -->
    
    <!-- <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> -->
    <title>Dashboard</title>
</head>
<body>
    <?php 
        $user = $this->session->userdata('user_info');
    ?>
    <div class="wrapper">
        <header>
            <div class="nav">
                <h3>Welcome, <?php echo $user['fname']; ?></h3>
                <div class="link">
                    <a href="<?php echo base_url().'DashboardController/logout'; ?>">Logout &nbsp; <i class="fa fa-sign-out"></i></a>
                </div>
            </div>
        </header>
        <sidebar>
            <div class="admin_info">
                <div class="circle">
                    <img src="<?php echo base_url().'images/men-s-blue-dress-shirt-1043471.jpg'; ?>">
                </div>
                <span class="online"></span>
                <p><?php echo $user['email']; ?></p>
            </div>
            <hr>
            <ul>
                <li><a href="<?= base_url().'dashboard'; ?>">Dashboard</a></li>
                <li><a href="<?= base_url().'create_post'; ?>">Add Article</a></li>
                <li><a href="<?= base_url().'posts'; ?>">View Posts</a></li>
                <li><a href="<?= base_url().'add_category'; ?>">Add Category</a></li>
                <li><a href="<?= base_url().'view_category'; ?>">View Categories</a></li>
            </ul>
        </sidebar>
        <main>
            <div class="content">
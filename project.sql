-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2020 at 03:20 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `a_id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`a_id`, `fname`, `lname`, `email`, `password`) VALUES
(1, 'Raj', 'Admin', 'raj@admin.com', 'cac15fb13b19762df9cb878740d8b60d9bb83433e1c5e7fbc8f59ba5e01578aa5e1f3eaea60578e4814cdfd14f8bdec2bb93644295b277933bb231e1ca2f286bn4TgEOgos0qvt6yWdbA/1EWqUqM27oq7XHcqtnZBzx4=');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `c_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`c_id`, `name`) VALUES
(4, 'Technology'),
(5, 'Sports'),
(6, 'Lifestyle'),
(7, 'Bollywood News');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `article_title` varchar(150) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `article_body` text NOT NULL,
  `author` int(11) NOT NULL,
  `publish_date` datetime NOT NULL,
  `categories` int(11) NOT NULL,
  `f_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `article_title`, `sub_title`, `article_body`, `author`, `publish_date`, `categories`, `f_image`) VALUES
(5, 'Six of My Online Stores Failed', 'Six of My Online Stores Failed', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pharetra sapien nibh, sed tempor mauris suscipit ac. Duis libero nibh, porttitor at posuere at, luctus et urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce et augue vitae nisl ultrices posuere. Sed facilisis metus rutrum, commodo lorem sed, consequat sem. Morbi enim orci, molestie a rutrum cursus, pellentesque nec ligula. Etiam porttitor iaculis purus sed imperdiet. Nulla consectetur eu sem ut dapibus. Duis nec viverra nisl, quis tincidunt massa.\r\n\r\nNam ut egestas dolor. Vestibulum ac dignissim sem, vel sollicitudin urna. Vivamus consequat est urna, eget feugiat orci porta fermentum. Maecenas pulvinar lacinia sapien. Fusce vitae orci tempus, congue lectus vel, luctus enim. Ut tempor felis eu ex accumsan volutpat. Vestibulum gravida vel est sed hendrerit. Curabitur finibus dignissim ipsum quis cursus. Sed eleifend non dui ac dictum. Nam dictum augue eget lorem ullamcorper, in iaculis urna dapibus. Nulla nunc ante, mattis in accumsan et, aliquet eget magna. Donec venenatis ut diam at condimentum. Mauris malesuada elit vel ante blandit scelerisque. Cras enim orci, luctus at consequat non, interdum ac ex. Donec quis venenatis eros.\r\n\r\nAliquam condimentum eu tortor eget feugiat. Maecenas tincidunt ultrices arcu venenatis rutrum. Suspendisse eu lectus eu erat aliquet tempus a ac urna. Integer euismod erat elementum magna pellentesque dapibus. Praesent interdum dolor felis, et auctor purus tempus rhoncus. Suspendisse cursus enim eu lacus semper, sed dictum neque tincidunt. Praesent pellentesque, quam vel congue accumsan, massa erat pulvinar velit, interdum fermentum nunc lacus posuere massa. Donec auctor semper metus, vel tempor neque cursus eu. Nam ultrices, ex sed dapibus dignissim, quam lorem auctor dui, eget varius felis erat lacinia lacus. Fusce vestibulum nec tortor in congue. Phasellus id quam efficitur, rutrum est at, hendrerit velit. Nulla laoreet ultricies metus quis pharetra. Curabitur libero ante, suscipit sit amet ex consectetur, mollis cursus magna.', 1, '2020-03-10 12:25:31', 6, 'preview2.jpg'),
(6, 'Perfecting the Art of Perfection', 'Perfecting the Art of Perfection', 'An Essay on Typography by Eric Gill takes the reader back to the year 1930. The year when a conflict between two worlds came to its term. The machines of the industrial world finally took over the handicrafts.\r\n\r\nThe typography of this industrial age was no longer handcrafted. Mass production and profit became more important. Quantity mattered more than the quality. The books and printed works in general lost a part of its humanity. The typefaces were not produced by craftsmen anymore. It was the machines printing and tying the books together now. The craftsmen had to let go of their craft and became a cog in the process. An extension of the industrial machine.\r\n\r\nBut the victory of the industrialism didn’t mean that the craftsmen were completely extinct. The two worlds continued to coexist independently. Each recognising the good in the other — the power of industrialism and the humanity of craftsmanship. This was the second transition that would strip typography of a part of its humanity. We have to go 500 years back in time to meet the first one.\r\n\r\nA similar conflict emerged after the invention of the first printing press in Europe. Johannes Gutenberg invented movable type and used it to produce different compositions. His workshop could print up to 240 impressions per hour. Until then, the books were being copied by hand. All the books were handwritten and decorated with hand drawn ornaments and figures. A process of copying a book was long but each book, even a copy, was a work of art.', 1, '2020-03-10 01:43:40', 5, 'preview11.jpg'),
(8, 'Installing Multiple Versions of Node.js Using NVM', 'Installing Multiple Versions of Node.js Using NVM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pharetra sapien nibh, sed tempor mauris suscipit ac. Duis libero nibh, porttitor at posuere at, luctus et urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce et augue vitae nisl ultrices posuere. Sed facilisis metus rutrum, commodo lorem sed, consequat sem. Morbi enim orci, molestie a rutrum cursus, pellentesque nec ligula. Etiam porttitor iaculis purus sed imperdiet. Nulla consectetur eu sem ut dapibus. Duis nec viverra nisl, quis tincidunt massa.\r\n\r\nNam ut egestas dolor. Vestibulum ac dignissim sem, vel sollicitudin urna. Vivamus consequat est urna, eget feugiat orci porta fermentum. Maecenas pulvinar lacinia sapien. Fusce vitae orci tempus, congue lectus vel, luctus enim. Ut tempor felis eu ex accumsan volutpat. Vestibulum gravida vel est sed hendrerit. Curabitur finibus dignissim ipsum quis cursus. Sed eleifend non dui ac dictum. Nam dictum augue eget lorem ullamcorper, in iaculis urna dapibus. Nulla nunc ante, mattis in accumsan et, aliquet eget magna. Donec venenatis ut diam at condimentum. Mauris malesuada elit vel ante blandit scelerisque. Cras enim orci, luctus at consequat non, interdum ac ex. Donec quis venenatis eros.\r\n\r\nAliquam condimentum eu tortor eget feugiat. Maecenas tincidunt ultrices arcu venenatis rutrum. Suspendisse eu lectus eu erat aliquet tempus a ac urna. Integer euismod erat elementum magna pellentesque dapibus. Praesent interdum dolor felis, et auctor purus tempus rhoncus. Suspendisse cursus enim eu lacus semper, sed dictum neque tincidunt. Praesent pellentesque, quam vel congue accumsan, massa erat pulvinar velit, interdum fermentum nunc lacus posuere massa. Donec auctor semper metus, vel tempor neque cursus eu. Nam ultrices, ex sed dapibus dignissim, quam lorem auctor dui, eget varius felis erat lacinia lacus. Fusce vestibulum nec tortor in congue. Phasellus id quam efficitur, rutrum est at, hendrerit velit. Nulla laoreet ultricies metus quis pharetra. Curabitur libero ante, suscipit sit amet ex consectetur, mollis cursus magna.\r\n\r\nSed nec eros feugiat, porta sapien non, porta metus. Morbi pharetra eros non commodo aliquet. Cras at magna massa. Proin rhoncus libero arcu, vehicula pretium dui egestas nec. Nunc ornare dui et egestas sodales. Mauris imperdiet risus ut ex pharetra cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla massa nulla, congue vitae nulla sit amet, fringilla laoreet felis. Donec risus ante, lobortis at vulputate eu, euismod sed metus. Duis eu molestie dui. Etiam gravida augue tortor, id congue ex luctus a. Donec ultrices id nisi in vulputate.\r\n\r\nProin dolor augue, accumsan id luctus ut, tristique eu ligula. Sed eu blandit tortor. Maecenas semper nunc eu augue semper, ac tempus leo blandit. Suspendisse nunc erat, tincidunt at diam nec, varius varius sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec accumsan, ipsum quis aliquet dapibus, quam purus fermentum ex, vel dignissim dolor nisl vulputate neque. Suspendisse sit amet tempus ipsum, quis vehicula eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.', 1, '2020-03-10 07:52:40', 5, 'preview36.jpg'),
(10, 'This is markdown unity test!', 'This is markdown unity test!', 'Documentation stored with source files shares the same permissions. Documentation stored in a separate Git repository can use different access controls. If Gerrit Code Review is being used, branch level read permissions can be used to grant or restrict access to any documentation branches.\r\nREADMEs\r\n\r\nFiles named README.md are automatically displayed below the file’s directory listing. For the top level directory this mirrors the standard GitHub presentation.\r\n\r\n*** promo README.md files are meant to provide orientation for developers browsing the repository, especially first-time users.\r\n\r\nWe recommend that Git repositories have an up-to-date top-level README.md file.\r\nMarkdown syntax\r\n\r\nGitiles supports the core Markdown syntax described in Markdown Basics. Additional extensions are supported to more closely match GitHub Flavored Markdown and simplify documentation writing.', 1, '2020-03-10 07:25:35', 6, 'preview2-1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`author`),
  ADD KEY `category` (`categories`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `category` FOREIGN KEY (`categories`) REFERENCES `category` (`c_id`),
  ADD CONSTRAINT `user` FOREIGN KEY (`author`) REFERENCES `admin` (`a_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php include('header.php'); ?>


<h2>Add Category</h2>
<hr>
<div class="message">
    <?php 
        if($this->session->flashdata('msg') != '')
        {
            if($this->session->flashdata('msg') == 'Category created successfully'){?>
            <div class="alert bg-success">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
            </div>
    <?php  }
        else
        {
        ?>
            <div class="alert bg-danger">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
            </div>
    <?php
        }
    }
    ?>
</div>
<div class="new_post_form">
    <form action="" method="POST" enctype="multipart/form-data" class="w-50 pt-5">
        <label>Category Name :</label>
        <div class="form-group">
            <input type="text" name="cat_name" class="form-control" required>
        </div>
        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
        <input type="submit" class="btn btn-primary" value="Create">
    </form>
</div>



<?php include('footer.php'); ?>
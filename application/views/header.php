<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Blog</title>
</head>
<body>
<header>
    <div class="content">
        <div class="wrap">
            <div class="logo">
                <a href="#" class="brand">Blog</a>
            </div>
            <div class="navbar_toggler">
                <a href="#" class="nav_icon"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>
            <ul class="nav">
                <li><a href="">Home</a></li>
                <li><a href="<?php echo base_url().'login'; ?>">Login</a></li>
            </ul>
        </div>
    </div>
</header>
<div class="wrapper">
    <main>
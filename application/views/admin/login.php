<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url().'css/admin.css'; ?>">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Login</title>
</head>
<body>
    <div class="wrapper">
        <div class="div1">
            
              <div class="intro">
                
                <h1>Administrative Area</h1>
                <h2>Dashboard | Login</h2>
              </div>
        </div>
        <div class="div2">
        <span class="circle1"></span>
        <span class="circle2"></span>
        <span class="circle3"></span>
            <?php 
                if($this->session->flashdata('error') != '')
                {?>
                    <div class="alert">
                        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                        <strong><?php echo $this->session->flashdata('error'); ?> !</strong> Please enter valid email or password.
                    </div>
            <?php  }
            ?>
            <div class="log_box">
                <form action="<?php echo base_url().'AdminController/authLogin'; ?>" method="post">
                    <label for="email">Email : </label>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email">
                    </div>
                    <label for="password">Password : </label>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password">
                    </div>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <button type="submit" class="login_btn">Login</button>
                </form>
            </div>
        </div>
        
    </div>
</body>
</html>
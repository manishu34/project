<?php include('header.php'); ?>
<div class="d-flex w-100">
<h2 style="flex:1;">All Posts</h2>
<a href="<?= base_url().'create_post'; ?>" class="btn btn-primary" style="height:40px;display:block;margin:5px 20px;"><i class="fa fa-plus"></i> Add Post</a>
</div>
<hr>
<div class="data">
<?php if($this->session->flashdata('msg') != ''){
    if($this->session->flashdata('msg') == 'Your post has been updated successfully'){
    ?>
<div class="alert bg-success">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
</div>
    <?php }
    else{ ?>
    <div class="alert bg-danger">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <strong class="text-white"><?php echo $this->session->flashdata('msg'); ?> !</strong>
</div>
    <?php 
    }
} ?>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Thumbnail</th>
                <th>Title</th>
                <th>Posted By</th>
                <th>Categories</th>
                <th>Published On</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <?php foreach($details as $post): ?>
            <tr>
                <td><img src="<?php echo base_url().'upload/'.$post->f_image; ?>" width="150px"></td>
                <td><?= $post->article_title; ?></td>
                <td><?= $post->fname; ?></td>
                <td><?= $post->name; ?></td>
                <td><?= $post->publish_date; ?></td>
                <td style="width:100px;text-align:center;">
                    <a href="<?php echo base_url().'update/'; ?><?php echo $post->id ;?>" class="btn btn-primary"><i class="fa fa-cog"></i></a>
                    <a href="<?php echo base_url().'deletePost/'; ?><?php echo $post->id ;?>" class="btn btn-danger"><i class="fa fa-close"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
    </table>
</div>


<?php include('footer.php'); ?>
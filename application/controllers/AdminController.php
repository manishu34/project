<?php

    class AdminController extends My_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('admin');
        }
        public function login(){

            $this->load->view('admin/login');
        }
        public function authLogin()
        {
            $this->load->library('encryption');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $user = $this->admin->loginCheck($email);
            if(!empty($user))
            {
                $hash_password = $this->encryption->decrypt($user->password);
                if($hash_password == $password)
                {
                    $user_info = [
                        'fname' => $user->fname,
                        'lname' => $user->lname,
                        'email' => $user->email,
                        'id' => $user->a_id
                    ];
                    $this->session->set_userdata('user_info', $user_info);
                    return redirect('dashboard');
                }
                else{
                    $this->session->set_flashdata('error', 'Incorrect Password');
                    return redirect('login');
                }
            }
            else{
                $this->session->set_flashdata('error', 'Invalid User');
                return redirect('login');
            }
        
        }
    }


?>
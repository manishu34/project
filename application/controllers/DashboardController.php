<?php

    class DashboardController extends My_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('dashboard');
            if(!$this->session->userdata('user_info'))
            {
                $this->session->set_flashdata('error', 'Try Logging In');
                return redirect('login');
            }
        }
        public function dashboard()
        {
            $this->load->view('dashboard/index');
        }
        public function logout()
        {
            $this->session->unset_userdata('user_info');
            return redirect('login');
        }
        public function category(){
            $name = $this->input->post('cat_name');
            if($name !=''){
                $insert = array(
                    'name' => $name
                );
                $check = $this->dashboard->addCategory($insert);
                if($check === True)
                {
                    $this->session->set_flashdata('msg', 'Category created successfully');
                    return redirect('add_category');
                }
                else{
                    $this->session->set_flashdata('msg', 'Something went wrong');
                    return redirect('add_category');
                }
            }
            $this->load->view('dashboard/add_category');
        }
        public function newPost(){
            $this->data['category'] = $this->dashboard->getCategory();
            $this->load->view('dashboard/add_post', $this->data);
        }
        
        public function doUpload()
        {
        
            $this->load->helper('date');
            $config = array(
                'upload_path' => './upload',
                'allowed_types' => 'png|jpeg|jpg'
            );
            $this->load->library('upload', $config);
            if($this->upload->do_upload())
            {
                $data = $this->input->post();
                $info = $this->upload->data();
                $image = $info['raw_name'].$info['file_ext'];
                $get_info = array(
                    'article_title' => $this->input->post('title'),
                    'sub_title' => $this->input->post('sub_title'),
                    'categories' => $this->input->post('category'),
                    'article_body' => $this->input->post('content'),
                    'f_image' => $image,
                    'publish_date' => date('Y-m-d h:i:s'),
                    'author' => $this->input->post('user_id')
                );
                $this->db->insert('posts',$get_info);
                $this->session->set_flashdata('msg', 'Your post has been published successfully');
                return redirect('create_post');
            }
            else 
            {
                $this->session->set_flashdata('msg', 'Publish Failed');
                return redirect('create_post');
            }
        }

        public function allPosts(){
            $this->data['details'] = $this->dashboard->getPosts();
            $this->load->view('dashboard/view_post', $this->data);
        }

        public function updatePost()
        {
            $post_id = $this->uri->segment(2);
            $this->data['single_post'] = $this->dashboard->fetch_single_data($post_id);
            $this->data['category'] = $this->dashboard->getCategory();
            $this->load->view('dashboard/update_post', $this->data);
        }

        public function categories(){
            $this->data['data'] = $this->dashboard->getCategory();
            $this->load->view('dashboard/view_categories', $this->data);
        }
        public function upadateCategory(){
            $cat = $this->uri->segment(2);
            $this->data['updateCategory'] = $this->dashboard->doUpdateCat($cat);
            $this->load->view('dashboard/update_category', $this->data);
        }
        public function doUpdateCategory(){
            $id = $this->input->post('c_id');
            $name = $this->input->post('c_name');
            if($name != ''){
                $data = [
                    'name' => $name
                ];
                $check = $this->dashboard->updateCategoryFuncton($data, $id);
                if($check == TRUE)
                {
                    $this->session->set_flashdata('msg', 'Category Updated Successfully');
                    return redirect('view_category');
                }else{
                    $this->session->set_flashdata('msg', 'Something went wrong');
                    return redirect('update_category');
                }
            }
        }

        public function doUpdatePost()
        {
            $this->load->helper('date');
            $config = array(
                'upload_path' => './upload',
                'allowed_types' => 'png|jpeg|jpg'
            );
            $this->load->library('upload', $config);
            if($this->upload->do_upload())
            {
                $data = $this->input->post();
                $id = $this->input->post('user_id');
                $info = $this->upload->data();
                $image = $info['raw_name'].$info['file_ext'];
                $get_info = array(
                    'article_title' => $this->input->post('title'),
                    'sub_title' => $this->input->post('sub_title'),
                    'categories' => $this->input->post('category'),
                    'article_body' => $this->input->post('content'),
                    'f_image' => $image,
                    'publish_date' => date('Y-m-d h:i:s')
                );
                $check = $this->dashboard->updatePostFunction($get_info, $id);
                if($check == TRUE){
                    $this->session->set_flashdata('msg', 'Your post has been updated successfully');
                    return redirect('posts');
                }
                else{
                    $this->session->set_flashdata('msg', 'Something went wrong');
                    return redirect('posts');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg', 'Invalid Filetype, use PNG, JPG or JPEG files only');
                return redirect('posts');
            }
        }

        public function deletePost()
        {
            $id = $this->uri->segment(2);
            $check = $this->dashboard->delPost($id);
            if($check == TRUE)
            {
                $this->session->set_flashdata('msg', 'Post Deleted Successfully');
                return redirect('posts');
            }
            else{
                $this->session->set_flashdata('msg', 'Failed to delete the post');
                return redirect('posts');
            }
            
        }
        public function deleteCategory()
        {
            $id = $this->uri->segment(2);
            $check = $this->dashboard->delCategory($id);
            if($check == TRUE)
            {
                $this->session->set_flashdata('msg', 'Category Deleted Successfully');
                return redirect('view_category');
            }
            else{
                $this->session->set_flashdata('msg', 'Failed to delete the category');
                return redirect('view_category');
            }
        }












    }


?>
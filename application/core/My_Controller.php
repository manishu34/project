<?php 


    class My_Controller extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
        }
        public function view($view)
        {
            $this->load->view('header');
            $this->load->view($view);
            $this->load->view('footer.php');
        }
        public function render($view, $data)
        {
            $this->load->view('header');
            $this->load->view($view, $data);
            $this->load->view('footer.php');
        }
    }

?>
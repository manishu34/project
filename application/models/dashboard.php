<?php 

    class Dashboard extends CI_Model
    {

        function addCategory($name)
        {
            $query = $this->db->insert('category', $name);
            return $query;
        }

        function getCategory()
        {
            $query = $this->db->GET('category');
            return $query->result();
        }

        function getPosts(){
            $this->db->select('*');    
            $this->db->from('posts');
            $this->db->join('admin', 'posts.author = admin.a_id');
            $this->db->join('category', 'posts.categories = category.c_id');
            $query = $this->db->get();
            return $query->result();
        }
        function fetch_single_data($id)
        {
            $this->db->select('*');    
            $this->db->from('posts');
            $this->db->where('id', $id);
            $this->db->join('admin', 'posts.author = admin.a_id');
            $this->db->join('category', 'posts.categories = category.c_id');
            $query = $this->db->get();

            return $query->result();
        }

        function doUpdateCat($id)
        {
            $this->db->SELECT('*');
            $this->db->WHERE('c_id', $id);
            $query = $this->db->GET('category');
            return $query->result();
        }

        function updateCategoryFuncton($name, $id)
        {
            $this->db->WHERE('c_id', $id);
            $query = $this->db->UPDATE('category', $name);
            return $query;
        }

        function updatePostFunction($data, $id){
            $this->db->WHERE('id', $id);
            $query = $this->db->UPDATE('posts', $data);
            return $query;
        }
        function delPost($id)
        {
            $this->db->WHERE('id', $id);
            $query = $this->db->DELETE('posts');
            return $query;
        }
        function delCategory($id)
        {
            $this->db->WHERE('c_id', $id);
            $query = $this->db->DELETE('category');
            return $query;
        }
        
    }

?>
<?php 

    class Blog extends CI_Model{

        function posts()
        {
            $this->db->SELECT('*');
            $this->db->FROM('posts');
            $this->db->JOIN('admin', 'posts.author = admin.a_id');
            $this->db->JOIN('category', 'posts.categories = category.c_id');
            $query = $this->db->get();
            
            return $query->result();
        }

        function category()
        {
            $query = $this->db->get('category');
            return $query->result();
        }
        function perCategory($id){
            $this->db->select('*');
            $this->db->from('posts');
            $this->db->join('category', 'posts.categories = category.c_id');
            $this->db->join('admin', 'posts.author = admin.a_id');
            $this->db->WHERE('categories', $id);
            $query = $this->db->get();
            return $query->result();
        }

        // function recentPosts()
        // {
        //     $sql = 'SELECT * FROM `posts` ORDER BY `posts`.`publish_date` DESC LIMIT 3 ';
        //     $query = $this->db->query($sql);
        //     return $query->result();
        // }

        function singleBlog($id)
        {
            $this->db->select('*');
            $this->db->from('posts');
            $this->db->join('category', 'posts.categories = category.c_id');
            $this->db->join('admin', 'posts.author = admin.a_id');
            $this->db->WHERE('id', $id);
            $query = $this->db->get();
            return $query->result();
        }

    }

?>